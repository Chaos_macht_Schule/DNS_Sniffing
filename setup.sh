#!/usr/bin/env bash

iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

echo '1' > /proc/sys/net/ipv4/ip_forward

nmcli r wifi off
rfkill unblock wifi

apt-get update
apt-get install -y hostapd dnsmasq etherape

systemctl unmask hostapd

mkdir -p /etc/hostapd

cp hostapd.conf /etc/hostapd/
cp dnsmasq.conf /etc/

ip addr add 10.0.0.1/24 dev wlan0

systemctl start hostapd
systemctl start dnsmasq

#!/usr/bin/env bash

while true
do
  awk '  BEGIN {printf "digraph dns_graph {\n"} /query/ {print "    \""$10"\" -> \""$8"\";"; ++cnt} END {printf "    \"Total\" -> \"" cnt "\";\n}\n"}' /var/log/dnsmasq.log > res.dot && dot -Tsvg -Ksfdp -o graph.svg res.dot
  sleep 0.5
done
